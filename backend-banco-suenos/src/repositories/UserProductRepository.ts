import { Service } from "typedi";
import { MongoBaseRepository } from "./base/MongoBaseRepository";
import { IRepository } from "./interfaces/IRepository";
import UserProduct, { IUserProduct } from "./model/UserProductModel";

export interface IUserProductRepository extends IRepository<IUserProduct> { }
@Service('userProductRepo')
export class UserProductRepository extends MongoBaseRepository<IUserProduct> implements IUserProductRepository{
  constructor() {
    super(UserProduct);
  }
}

Object.seal(UserProductRepository);