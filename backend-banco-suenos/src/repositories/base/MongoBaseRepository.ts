
import * as mongoose from 'mongoose';
import { IRepository } from '../interfaces/IRepository';

export class MongoBaseRepository<T extends mongoose.Document> implements IRepository<T>  {

  private _model: mongoose.Model<mongoose.Document>;

  constructor(schemaModel: mongoose.Model<mongoose.Document>) {
    this._model = schemaModel;
  }

  getName(): string {
    return 'Repositorio MongoBD';
  }

  create(item: any, callback?: (error: any, result: T) => void): Promise<T> {
    const self = this;
    const p = new Promise<T>((resolve, reject) => {
      self._model.create(item, (err: any, res: T) => {
        if (callback) {
          callback(err, res as T);
        }
        if (err) {
          reject(err);
        }
        else {
          resolve(res as T);
        }
      });
    });

    return p;
  }

  findById(id: string, callback?: (error: any, result: T) => void): Promise<T> {
    const self = this;
    const p = new Promise<T>((resolve, reject) => {
      self._model.findById(id, (err, res) => {
        if (callback) {
          callback(err, res as T);
        }
        if (err) {
          reject(err);
        }
        else {
          resolve(res as T);
        }
      });
    });

    return p;
  }

  findOneAndRemove(cond: any, options: any, callback?: (err: any, res: T) => void): Promise<T> {
    const self = this;
    const p = new Promise<T>((resolve, reject) => {
      self._model.findOneAndRemove(cond, options, callback).exec((err, res) => {
        if (callback) {
          callback(err, res as T);
        }
        if (err) {
          reject(err);
        }
        else {
          resolve(res as T);
        }
      });
    });

    return p;
  }


  findOne(cond: any, fields: any, options: any, callback?: (err: any, res: T) => void): Promise<T> {
    const self = this;
    const p = new Promise<T>((resolve, reject) => {
      self._model.findOne(cond, fields, options).exec((err, res) => {
        if (callback) {
          callback(err, res as T);
        }
        if (err) {
          reject(err);
        }
        else {
          resolve(res as T);
        }
      });
    });

    return p;
  }

  find(cond: any, fields: any, options: any, sortOptions?: any, callback?: (err: any, res: T[]) => void): Promise<T[]> {
    const p = new Promise<T[]>((resolve, reject) => {
      let query = this._model.find(cond, fields, options);
      if (sortOptions) {
        query = query.sort(sortOptions);
      }

      query.exec((err, res) => {
        if (callback) {
          callback(err, res as T[]);
        }
        if (err) {
          reject(err);
        }
        else {
          resolve(res as T[]);
        }
      });
    });

    return p;
  }

  count(cond?: any): Promise<number> {
    const self = this;
    const p = new Promise<number>((resolve, reject) => {
      self._model.count(cond, (err, count) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(count);
        }
      });
    });

    return p;
  }

  save(item: T, callback?: (error: any, result: T) => void): Promise<T> {
    const p = new Promise<T>((resolve, reject) => {
      item.save((err, result) => {
        if (callback) {
          callback(err, result as T);
        }

        if (err) {
          reject(err);
        }
        else {
          resolve(result as T);
        }
      });
      resolve(null);
    });

    return p;
  }

  upsert(cond: any, item: any, callback?: (error: any, result: T) => void): Promise<T> {
    const self = this;
    const p = new Promise<T>((resolve, reject) => {
      const options = {
        upsert: true
      };
      self._model.findOneAndUpdate(cond, item, options, (err, result) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(result as T);
        }
      });
    });

    return p;
  }

}
