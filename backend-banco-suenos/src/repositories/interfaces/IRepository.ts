import * as mongoose from 'mongoose';
import { IRead } from "./IRead";
import { IWrite } from "./IWrite";

export interface IRepository<T extends mongoose.Document> extends IRead<T>, IWrite<T> {
    getName: () => string;
  }