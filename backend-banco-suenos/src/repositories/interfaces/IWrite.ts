import * as mongoose from 'mongoose';

export interface IWrite<T extends mongoose.Document> {
    create: (item: any, callback?: (error: any, result: T) => void) => Promise<T>;
    save: (item: T, callback?: (error: any, result: T) => void) => Promise<T>;
    upsert: (cond: any, item: T, callback?: (error: any, result: T) => void) => Promise<T>;
    findOneAndRemove(cond: any, options: any, callback?: (err: any, res: T) => void): Promise<T>;
}