import { Service } from "typedi";
import { MongoBaseRepository } from "./base/MongoBaseRepository";
import { IRepository } from "./interfaces/IRepository";
import Transaction, { ITransaction } from "./model/TransactionModel";

export interface ITransactionRepository extends IRepository<ITransaction> { }
@Service('transactionRepo')
export class TransactionRepository extends MongoBaseRepository<ITransaction> implements ITransactionRepository {
  constructor() {
    super(Transaction);
  }
}

Object.seal(TransactionRepository);