import { Service } from "typedi";
import { MongoBaseRepository } from "./base/MongoBaseRepository";
import { IRepository } from "./interfaces/IRepository";
import BankAccount, { IBankAccount } from "./model/BankAccountModel";

export interface IBankAccountRepository extends IRepository<IBankAccount> { }

@Service('banckAccountRepo')
export class BankAccountRepository extends MongoBaseRepository<IBankAccount> implements IBankAccountRepository {
  constructor() {
    super(BankAccount);
  }
}

Object.seal(BankAccountRepository);