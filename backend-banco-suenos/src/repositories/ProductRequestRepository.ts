import { Service } from "typedi";
import { MongoBaseRepository } from "./base/MongoBaseRepository";
import { IRepository } from "./interfaces/IRepository";
import ProductRequest, { IProductRequest } from "./model/ProductRequestModel";

export interface IProductRequestRepository extends IRepository<IProductRequest> { }
@Service('productRequestRepo')
export class ProductRequestRepository extends MongoBaseRepository<IProductRequest> implements IProductRequestRepository{
  constructor() {
    super(ProductRequest);
  }
}

Object.seal(ProductRequestRepository);