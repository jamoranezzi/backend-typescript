import mongoose, { Schema, Document } from 'mongoose';

// Exportar IProduct
export interface IProduct extends Document {
    code: string;
    name: string;
    createdAt: Date;
    modifiedAt: Date;
}

const ProductSchema: Schema = new Schema({
    code: { type: String, unique: true, required: [true, 'El codigo es necesario'] },
    name: { type: String, required: [true, 'El nombre es necesario'] },
    createdAt: { type: Date, required: false },
    modifiedAt: { type: Date, required: false }
}, { collection: 'Product' }).pre<IProduct>('save', function (next) {
    if (this.isNew) {
        this.createdAt = new Date();
    } else {
        this.modifiedAt = new Date();
    }
    next();
});

// Exportar modelo y esquema
export default mongoose.model<IProduct>('Product', ProductSchema);