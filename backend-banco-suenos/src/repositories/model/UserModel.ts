
import mongoose, { Schema, Document } from 'mongoose';

// Exportar IUser
export interface IUser extends Document {
  name: string;
  cedula: string;
  password: string;
  email: string;
  createdAt: Date;
  modifiedAt: Date;
}

const UserSchema: Schema = new Schema({
  name: { type: String, required: [true, 'El nombre es necesario'] },
  cedula: { type: String, unique: true, required: [true, 'La Cedula es requerida'] },
  password: { type: String, required: [true, 'la contraseña es necesaria'] },
  email: { type: String, required: false },
  createdAt: { type: Date, required: false },
  modifiedAt: { type: Date, required: false }
}, { collection: 'User' }).pre<IUser>('save', function (next) {
  if (this.isNew) {
    this.createdAt = new Date();
  } else {
    this.modifiedAt = new Date();
  }
  next();
});

// Exportar modelo y esquema
export default mongoose.model<IUser>('User', UserSchema);
