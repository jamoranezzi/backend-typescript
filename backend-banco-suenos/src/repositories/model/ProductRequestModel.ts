
import mongoose, { Schema, Document } from 'mongoose';

// Exportar IProductRequest
export interface IProductRequest extends Document {
    user: Schema.Types.ObjectId;
    product: Schema.Types.ObjectId;
    status: string;
    createdAt: Date;
    modifiedAt: Date;
}

const ProductRequestSchema: Schema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: [true, 'El usuario es necesario'] },
    product: { type: Schema.Types.ObjectId, ref: 'Product', required: [true, 'El producto es necesario'] },
    status: { type: String, default: 'PENDIENTE', required: [true, 'El estado es necesario'] },
    createdAt: { type: Date, required: false },
    modifiedAt: { type: Date, required: false }
}, { collection: 'ProductRequest' }).pre<IProductRequest>('save', function (next) {
    if (this.isNew) {
        this.createdAt = new Date();
    } else {
        this.modifiedAt = new Date();
    }
    next();
});

// Exportar modelo y esquema
export default mongoose.model<IProductRequest>('ProductRequest', ProductRequestSchema);
