
import mongoose, { Schema, Document } from 'mongoose';

// Exportar IBankAccount
export interface IBankAccount extends Document {
    accountNumber: string;
    user: Schema.Types.ObjectId;
    product: Schema.Types.ObjectId;
    createdAt: Date;
    modifiedAt: Date;
}

const BankAccountSchema: Schema = new Schema({
    accountNumber: { type: String, unique: true, required: [true, 'El número de cuenta es necesario'] },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: [true, 'El id del usuario es necesario'] },
    product: { type: Schema.Types.ObjectId, ref: 'Product', required: [true, 'El id del producto es necesario'] },
    createdAt: { type: Date, required: false },
    modifiedAt: { type: Date, required: false }
}, { collection: 'BankAccount' }).pre<IBankAccount>('save', function (next) {
    if (this.isNew) {
        this.createdAt = new Date();
    } else {
        this.modifiedAt = new Date();
    }
    next();
});

// Exportar modelo y esquema
export default mongoose.model<IBankAccount>('BankAccount', BankAccountSchema);
