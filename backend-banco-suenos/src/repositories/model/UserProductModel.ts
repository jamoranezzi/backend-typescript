import mongoose, { Schema, Document } from 'mongoose';

// Exportar IUserProduct
export interface IUserProduct extends Document {
    user: Schema.Types.ObjectId;
    product: Schema.Types.ObjectId;
    createdAt: Date;
    modifiedAt: Date;
}

const UserProductSchema: Schema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: [true, 'El usuario es necesario'] },
    product: { type: Schema.Types.ObjectId, ref: 'Product', required: [true, 'El producto es necesario'] },
    createdAt: { type: Date, required: false },
    modifiedAt: { type: Date, required: false }
}, { collection: 'UserProduct' }).pre<IUserProduct>('save', function (next) {
    if (this.isNew) {
        this.createdAt = new Date();
    } else {
        this.modifiedAt = new Date();
    }
    next();
});

// Exportar modelo y esquema
export default mongoose.model<IUserProduct>('UserProduct', UserProductSchema);
