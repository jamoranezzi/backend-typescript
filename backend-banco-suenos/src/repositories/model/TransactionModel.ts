import mongoose, { Schema, Document } from 'mongoose';

// Exportar ITransaction
export interface ITransaction extends Document {
    account: Schema.Types.ObjectId;
    commerce: string;
    amount: number;
    status: string;
    createdAt: Date;
    modifiedAt: Date;
}

const TransactionSchema: Schema = new Schema({
    account: { type: Schema.Types.ObjectId, ref: 'BankAccount', required: [true, 'El id de número de cuenta es requerido'] },
    commerce: { type: String, required: [true, 'El comercio es necesario'] },
    amount: { type: Number, required: [true, 'El monto es necesario'] },
    status: { type: String, required: [true, 'El estado es necesario'] },
    createdAt: { type: Date, required: false },
    modifiedAt: { type: Date, required: false }
}, { collection: 'Transaction' }).pre<ITransaction>('save', function (next) {
    if (this.isNew) {
        this.createdAt = new Date();
    } else {
        this.modifiedAt = new Date();
    }
    next();
});

// Exportar modelo y esquema
export default mongoose.model<ITransaction>('Transaction', TransactionSchema);