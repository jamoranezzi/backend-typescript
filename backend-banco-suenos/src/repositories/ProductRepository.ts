import { Service } from "typedi";
import { MongoBaseRepository } from "./base/MongoBaseRepository";
import { IRepository } from "./interfaces/IRepository";
import Product, { IProduct } from "./model/ProductModel";

export interface IProductRepository extends IRepository<IProduct> { }

@Service('productRepo')
export class ProductRepository extends MongoBaseRepository<IProduct> implements IProductRepository{
  constructor() {
    super(Product);
  }
}

Object.seal(ProductRepository);