import {Service,Inject, Container} from "typedi";
import { MongoBaseRepository } from "./base/MongoBaseRepository";
import { IRepository } from "./interfaces/IRepository";
import User, { IUser } from "./model/UserModel";

export interface IUserRepository extends IRepository<IUser> { }

@Service('userRepo')
export class UserRepository extends MongoBaseRepository<IUser> implements IUserRepository{
  constructor() {
    super(User);
  }
}

Object.seal(UserRepository);