import "reflect-metadata";
import { Container } from "typedi";
import { UserRepository } from "./repositories/UserRespository";
import { ProductRepository } from "./repositories/ProductRepository";
import { BankAccountRepository } from "./repositories/BankAccountRepository";
import { TransactionRepository } from "./repositories/TransactionRepository";
import { UserProductRepository } from "./repositories/UserProductRepository";
import { ProductRequestRepository } from "./repositories/ProductRequestRepository";
import dotenv from "dotenv";
import express from "express";
import path from "path";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import routes from "./routes";

// inicializar configuracion
dotenv.config();

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;
const app = express();

// CORS
app.use((req, res, next) => {
    res.header(process.env.ACCESS_CONTROL_ALLOW_ORIGIN_TEXT, process.env.ACCESS_CONTROL_ALLOW_ORIGIN);
    res.header(process.env.ACCESS_CONTROL_ALLOW_HEADERS_TEXT, process.env.ACCESS_CONTROL_ALLOW_HEADERS);
    res.header(process.env.ACCESS_CONTROL_ALLOW_METHODS_TEXT, process.env.ACCESS_CONTROL_ALLOW_METHODS);
    next();
});

// Body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Configure uso de EJS
app.set( "views", path.join( __dirname, "views" ) );
app.set( "view engine", "ejs" );

// DI setup
Container.set('userRepo', UserRepository);
Container.set('productRepo', ProductRepository);
Container.set('banckAccountRepo', BankAccountRepository);
Container.set('transactionRepo', TransactionRepository);
Container.set('userProductRepo', UserProductRepository);
Container.set('productRequestRepo', ProductRequestRepository);

// Conexión a la base de datos
mongoose.connect(process.env.MONGODB_HOST,  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  }).then(() => {
     // tslint:disable-next-line:no-console
    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
}).catch((err) => {
    // tslint:disable-next-line:no-console
    console.log(err);
});

// Configurar routes
app.use("/api", routes);

// Iniciar Express server
app.listen( port, () => {
     // tslint:disable-next-line:no-console
    console.log(`Express Server puerto ${ port }: \x1b[32m%s\x1b[0m`, 'online')
} );