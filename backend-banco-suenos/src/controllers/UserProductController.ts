import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import { IUserProduct } from '../repositories/model/UserProductModel';
import { IUserProductRepository } from '../repositories/UserProductRepository';


interface IUserProductInput {
    user: IUserProduct['user'];
    product: IUserProduct['product'];
}
@Service()
export class UserProductController {

    @Inject('userProductRepo')
    private _repository: IUserProductRepository;


    async addProductToUser(req: Request, res: Response): Promise<any> {
        try {
            const userProduct = {
                user: req.body.user._id,
                product: req.body.product
            } as IUserProductInput;

            const createdUserProduct = await this._repository.create(userProduct);

            return res.status(201).json({
                ok: true,
                product: createdUserProduct
            });
        } catch (error) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creando el Producto',
                err: error
            });
        }
    }

    async getAllByUser(req: Request, res: Response): Promise<any> {
        try {
            const userProducts = await this._repository.find({
                user: req.body.user._id
            }, {}, {});


            if (userProducts.length === 0) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontro productos para el Usuario'
                });
            }

            for (const userProduct of userProducts) {
                await userProduct.populate('product', 'code name').execPopulate();
            }

            return res.status(200).json({
                ok: true,
                count: userProducts.length,
                userProducts
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Productos',
                err: error
            });
        }

    }

}