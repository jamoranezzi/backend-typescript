import { IProduct } from '../repositories/model/ProductModel';
import { Request, Response } from "express";
import { IProductRepository } from '../repositories/ProductRepository';
import { Service, Inject } from "typedi";

interface ICreateProductInput {
    code: IProduct['code'];
    name: IProduct['name'];
}

interface IUpdateProductInput {
    code: IProduct['code'];
    name?: IProduct['name'];
}

@Service()
export class ProductController {

    @Inject('productRepo')
    private _repository: IProductRepository;

    async createProduct(req: Request, res: Response): Promise<any> {
        try {
            const product = {
                code: req.body.code,
                name: req.body.name
            }as ICreateProductInput;

          const createdProduct = await this._repository.create(product);

          return res.status(201).json({
            ok: true,
            product: createdProduct
          });
        } catch (error) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creando Producto',
                err: error
            });
        }
    }

    async  updateProduct(req: Request, res: Response): Promise<any> {
        try {
            const productRequest = {
                code: req.body.code,
                name: req.body.name
            } as IUpdateProductInput;

            let product = await this._repository.findOne({ code: productRequest.code }, {}, {});
            if (product) {
                product.code = !!productRequest.code ? productRequest.code : product.code;
                product.name = !!productRequest.name ? productRequest.name : product.name;
                await product.save();

                return res.status(200).json({
                    ok: true,
                    product
                });
            }
            product = await this._repository.create(productRequest);

            return res.status(200).json({
                ok: true,
                product
            });
        } catch (error) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al actualizar Producto',
                err: error
            });

        }
    }

    async  getAllProducts(req: Request, res: Response): Promise<any> {
        try {
            const product = await this._repository.find({}, 'code name createdAt', {});
            if(product.length === 0){
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontraron Productos'
                });
            }
            return res.status(200).json({
                ok: true,
                product
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Producto',
                err: error
            });
        }
    }

    async  getProduct(req: Request, res: Response): Promise<any> {
        try {
            const product = await this._repository.findOne({ code: req.params.code }, 'code name createdAt', {});
            if(!product){
                return res.status(404).json({
                    ok: false,
                    mensaje: 'Producto no encontrado'
                });
            }

            return res.status(200).json({
                ok: true,
                product
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Producto',
                err: error
            });
        }
    }

    async deleteProduct(req: Request, res: Response): Promise<any> {
        try {
            const product = await this._repository.findOneAndRemove({ code: req.params.code}, {});
            if(!product){
                return res.status(404).json({
                    ok: false,
                    mensaje: 'Producto no encontrado'
                });
            }

            return res.status(200).json({
                ok: true,
                product
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar Producto',
                err: error
            });
        }
    }
}
