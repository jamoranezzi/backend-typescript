import { IUser } from '../repositories/model/UserModel';
import { IUserRepository } from '../repositories/UserRespository';
import { Request, Response } from "express";
import * as bcrypt from 'bcrypt';
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { Inject, Service } from 'typedi';

interface ILoginInput {
    cedula: IUser['cedula'];
    password: IUser['password'];
}

@Service()
export class AuthController {

    @Inject('userRepo')
    private _repository: IUserRepository;

    async login(req: Request, res: Response): Promise<any> {

        const loginData = {
            cedula: req.body.cedula,
            password: req.body.password,
        } as ILoginInput;

        const user = await this._repository.findOne({ cedula: loginData.cedula }, 'name cedula email password', {});
        if (!user) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales Incorrectas - cedula',
                err: { message: 'la cedula es incorrecta' }
            });
        }

        if (!bcrypt.compareSync(loginData.password, user.password)) {
            return res.status(401).json({
                ok: false,
                mensaje: 'Credenciales Incorrectas - Password',
                err: { message: 'El Password es incorrecto' }
            });
        }

        // Sing JWT, valido por una hora
        const token = jwt.sign(
            {
                user: {
                    _id: user._id,
                    name: user.name,
                    cedula: user.cedula,
                    email: user.email
                }
            },
            config.jwtSecret,
            { expiresIn: "1h" }
        );

        return res.status(200).json({
            ok: true,
            usuario: user,
            token
        });
    }
}