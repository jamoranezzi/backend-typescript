import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import { ITransaction } from '../repositories/model/TransactionModel';
import { ITransactionRepository } from '../repositories/TransactionRepository';


interface ICreateTransactionInput {
    account: ITransaction['account'];
    commerce: ITransaction['commerce'];
    amount: ITransaction['amount'];
    status: ITransaction['status'];
}

@Service()
export class TransactionController {

    @Inject('transactionRepo')
    private _repository: ITransactionRepository;

    async createTransaction(req: Request, res: Response): Promise<any> {
        try {
            const transaction = {
                account: req.body.account,
                commerce: req.body.commerce,
                amount: req.body.amount,
                status: req.body.status
            } as ICreateTransactionInput;

            const createdTransaction = await this._repository.create(transaction);

            return res.status(201).json({
                ok: true,
                transaction: createdTransaction
            });
        } catch (error) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creando Transacción',
                err: error
            });
        }
    }

    async getAllTransactionsByAccount(req: Request, res: Response): Promise<any> {
        try {
            const transactions = await this._repository.find({
                account: req.body.account
            }, {}, {});

            if (transactions.length === 0) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontraron Transacciones asociadas a la cuenta'
                });
            }
            let totalAmount = 0;
            for (const transaction of transactions) {
                totalAmount += transaction.amount;
                await transaction.populate('account', 'accountNumber user product').execPopulate();
            }

            const average = (totalAmount/transactions.length).toFixed(2);
            return res.status(200).json({
                ok: true,
                count: transactions.length,
                average,
                transactions,
            });

        } catch (error) {
            // tslint:disable-next-line:no-console
            console.log(error);
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar transacciones',
                err: error
            });
        }

    }

}