import { IBankAccountRepository } from '../repositories/BankAccountRepository';
import { Request, Response } from "express";
import { IBankAccount } from '../repositories/model/BankAccountModel';
import { Inject, Service } from 'typedi';


interface IBankAccountInput {
    accountNumber: IBankAccount['accountNumber'];
    product: IBankAccount['product'];
}

@Service()
export class BankAccountController {

    @Inject('banckAccountRepo')
    private _repository: IBankAccountRepository;

    async createBankAccount(req: Request, res: Response): Promise<any> {
        try {
            const banckAccount = {
                accountNumber: req.body.accountNumber,
                user: req.body.user._id,
                product: req.body.product
            } as IBankAccountInput;

            const createdBankAccount = await this._repository.create(banckAccount);

            return res.status(201).json({
                ok: true,
                banckAccount: createdBankAccount
            });
        } catch (error) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creando Cuenta Bancaria',
                err: error
            });
        }
    }

    async getAllByUser(req: Request, res: Response): Promise<any> {
        try {
            const bankAccounts = await this._repository.find({
                user: req.body.user._id
            }, {}, {});


            if (bankAccounts.length === 0) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontraron Cuentas Bancarias'
                });
            }

            for (const bankAccount of bankAccounts) {
                await bankAccount.populate('user', 'name cedula').populate('product', 'code name').execPopulate();
            }

            return res.status(200).json({
                ok: true,
                bankAccounts
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Cuentas Bancarias',
                err: error
            });
        }

    }

    async getByAccountNumber(req: Request, res: Response): Promise<any> {
        try {
            const bankAccount = await this._repository.findOne(
                {
                    user: req.body.user._id,
                    accountNumber: req.params.accountNumber
                }, {}, {});

            if (!bankAccount) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontro Cuenta Bancaria'
                });
            }

            await bankAccount.populate('user', 'name cedula').populate('product', 'code name').execPopulate();

            return res.status(200).json({
                ok: true,
                bankAccount
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Cuenta Bancaria',
                err: error
            });
        }
    }

    async deleteAccountNumber(req: Request, res: Response): Promise<any> {
        try {
            const bankAccount = await this._repository.findOneAndRemove(
                {
                    user: req.body.user._id,
                    accountNumber: req.params.accountNumber
                }, {});

            if (!bankAccount) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontro Cuenta Bancaria'
                });
            }

            return res.status(200).json({
                ok: true,
                bankAccount
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al eliminar Cuenta Bancaria',
                err: error
            });
        }
    }
}