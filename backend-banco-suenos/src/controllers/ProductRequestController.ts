import { Request, Response } from "express";
import { IProductRequestRepository } from "../repositories/ProductRequestRepository";
import { IProductRequest } from "../repositories/model/ProductRequestModel";
import { IUserProductRepository } from "../repositories/UserProductRepository";
import { Inject, Service } from "typedi";


interface IProducRequesttInput {
    user: IProductRequest['user'];
    product: IProductRequest['product'];
}

@Service()
export class ProducRequestController {

    @Inject('productRequestRepo')
    private _repository: IProductRequestRepository;

    @Inject('userProductRepo')
    private userProductRepository: IUserProductRepository;

    async addProductRequest(req: Request, res: Response): Promise<any> {
        try {
            const userProduct = {
                user: req.body.user._id,
                product: req.body.product
            } as IProducRequesttInput;

            const createdProductRequest = await this._repository.create(userProduct);

            return res.status(201).json({
                ok: true,
                request: createdProductRequest
            });
        } catch (error) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creando Solicitud de request',
                err: error
            });
        }
    }

    async getAllRequestByUser(req: Request, res: Response): Promise<any> {
        try {
            const userProductRequests = await this._repository.find({
                user: req.body.user._id
            }, {}, {});


            if (userProductRequests.length === 0) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontro solicitudes de productos nuevos'
                });
            }

            for (const userProductRequest of userProductRequests) {
                await userProductRequest.populate('product', 'code name').execPopulate();
            }

            return res.status(200).json({
                ok: true,
                count: userProductRequests.length,
                user_request: userProductRequests
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar solicitudes de productos',
                err: error
            });
        }

    }

    async getRequestById(req: Request, res: Response): Promise<any> {
        try {
            const userProductRequest = await this._repository.findOne({
                user: req.body.user._id,
                _id: req.params.id
            }, {}, {});

            if (!userProductRequest) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontro la  solicitud de producto'
                });
            }
            await userProductRequest.populate('user', 'name cedula').populate('product', 'code name').execPopulate();

            // Buscamos las transacciones permitidas del usuario
            const usertProducts = await this.userProductRepository.find({ user: req.body.user._id }, 'product', {});
            for (const usertProduct of usertProducts) {
                await usertProduct.populate('product', 'code name').execPopulate();
            }

            return res.status(200).json({
                ok: true,
                user_request: userProductRequest,
                usertProducts
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar solicitud de producto',
                err: error
            });
        }

    }

    async deleteProductRequest(req: Request, res: Response): Promise<any> {
        try {
            const userProductRequest = await this._repository.findOneAndRemove(
                {
                    _id: req.params.id
                }, {});

            if (!userProductRequest) {
                return res.status(404).json({
                    ok: false,
                    mensaje: 'No se encontro la Solicitud'
                });
            }

            return res.status(200).json({
                ok: true,
                user_request: userProductRequest
            });
        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al eliminar la Solicitud',
                err: error
            });
        }
    }

}