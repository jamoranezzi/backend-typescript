import { IUser } from '../repositories/model/UserModel';
import { Request, Response } from "express";
import { IUserRepository, UserRepository } from '../repositories/UserRespository';
import * as bcrypt from 'bcrypt';
import { Service, Inject } from "typedi";

interface ICreateUserInput {
  name: IUser['name'];
  cedula: IUser['cedula'];
  password: IUser['password'];
  email?: IUser['email'];
}
interface IUpdateUserInput {
  name?: IUser['name'];
  cedula: IUser['cedula'];
  password?: IUser['password'];
  email?: IUser['email'];
}
@Service()
export class UserController {

  @Inject('userRepo')
  private _repository: IUserRepository;

  async createUser(req: Request, res: Response): Promise<any> {
    try {
      const user = {
        name: req.body.name,
        cedula: req.body.cedula,
        password: !!req.body.password ? bcrypt.hashSync(req.body.password, 10) : req.body.password,
        email: req.body.email
      } as ICreateUserInput;

      const createdUser = await this._repository.create(user);

      return res.status(201).json({
        ok: true,
        user: createdUser
      });
    } catch (error) {
      return res.status(400).json({
        ok: false,
        mensaje: 'Error creando usuario',
        err: error
      });
    }
  }

  async updateUser(req: Request, res: Response): Promise<any> {
    try {
      const userRequest = {
        name: req.body.name,
        cedula: req.body.cedula,
        password: !!req.body.password ? bcrypt.hashSync(req.body.password, 10) : req.body.password,
        email: req.body.email
      } as IUpdateUserInput

      let user = await this._repository.findOne({ cedula: userRequest.cedula }, {}, {});
      if (user) {
        user.name = !!userRequest.name ? userRequest.name : user.name;
        user.cedula = !!userRequest.cedula ? userRequest.cedula : user.cedula;
        user.password = !!userRequest.password ? userRequest.password : user.password;
        user.email = !!userRequest.email ? userRequest.email : user.email;
        await user.save();
        return res.status(200).json({
          ok: true,
          user
        });
      }

      user = await this._repository.create(userRequest);

      return res.status(200).json({
        ok: true,
        user
      });
    } catch (error) {
      return res.status(400).json({
        ok: false,
        mensaje: 'Error al actualizar usuario',
        err: error
      });

    }
  }

  async getAllUsers(req: Request, res: Response): Promise<any> {
    try {
      const users = await this._repository.find({}, 'name cedula email createdAt', {});
      if (users.length === 0) {
        return res.status(404).json({
          ok: false,
          mensaje: 'No se encontraron Usuarios'
        });
      }

      return res.status(200).json({
        ok: true,
        users
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        mensaje: 'Error al buscar usuario',
        err: error
      });
    }
  }

  async getUser(req: Request, res: Response): Promise<any> {
    try {
      const user = await this._repository.findOne({ cedula: req.params.cedula }, 'name cedula email createdAt', {});
      if (!user) {
        return res.status(404).json({
          ok: false,
          mensaje: 'Usuario no encontrado'
        });
      }
      return res.status(200).json({
        ok: true,
        user
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        mensaje: 'Error al buscar usuario',
        err: error
      });
    }
  }

  async deleteUser(req: Request, res: Response): Promise<any> {
    try {
      const user = await this._repository.findOneAndRemove({ cedula: req.params.cedula }, {});
      if (!user) {
        return res.status(404).json({
          ok: false,
          mensaje: 'Usuario no encontrado'
        });
      }

      return res.status(200).json({
        ok: true,
        user
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        mensaje: 'Error al borrar usuario',
        err: error
      });
    }
  }
}
