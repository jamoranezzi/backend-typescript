import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {

  if (!req.headers.authorization) {
      return res.status(401).json({
        ok: false,
        mensaje: 'Tu petición no tiene cabecera de autorización'
      });
  }
  // Obtener token del header
  const token = req.headers.authorization.split(" ")[1] as string;
  let jwtPayload;

  // Validar token y obtener informacion
  try {
    jwtPayload = (jwt.verify(token, config.jwtSecret) as any);
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    // Si el token es invalido response 401 (unauthorized)
    return res.status(401).json({
      ok: false,
      mensaje: 'Token incorrecto',
      err: error
    });
  }

  // Enviar un nuevo token con cada peticion
  const { user } = jwtPayload;
  req.body.user = jwtPayload.user;
  const newToken = jwt.sign({ user }, config.jwtSecret, {
    expiresIn: "1h"
  });
  res.setHeader("token", newToken);

  next();
};