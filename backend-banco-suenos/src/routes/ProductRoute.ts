import "reflect-metadata";
import { Container  } from "typedi";
import { checkJwt } from "../middleware/CheckJwt";
import { Router } from "express";
import { ProductController } from "../controllers/ProductController";

const router = Router();

// CDI
const productController =  Container.get(ProductController);
//

// =================================
// Obtener todos los Productos
// =================================
router.get('/all', checkJwt, productController.getAllProducts.bind(productController));

// =================================
// Obtener Producto
// =================================
router.get('/:code', checkJwt, productController.getProduct.bind(productController));

// =================================
// Crear Producto
// =================================
router.post('/', checkJwt, productController.createProduct.bind(productController));

// =================================
// Actualizar Producto
// =================================
router.put('/', checkJwt, productController.updateProduct.bind(productController));

// =================================
// Eliminar Producto
// =================================
router.delete('/:code', checkJwt, productController.deleteProduct.bind(productController));

export default router;
