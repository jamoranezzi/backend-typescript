import "reflect-metadata";
import { Container  } from "typedi";
import { Router } from "express";
import { BankAccountController } from "../controllers/BankAccountController";
import { checkJwt } from "../middleware/CheckJwt";

const router = Router();

// CDI
const bankAccountController = Container.get(BankAccountController);
//

// =================================
// Crear Cuenta Bancaria
// =================================
router.post('/', checkJwt, bankAccountController.createBankAccount.bind(bankAccountController));

// =================================
// Obtener todas las cuentas de un usuario
// =================================
router.get('/all', checkJwt, bankAccountController.getAllByUser.bind(bankAccountController));

// =================================
// Obtener cuenta del usurio
// =================================
router.get('/:accountNumber', checkJwt, bankAccountController.getByAccountNumber.bind(bankAccountController));

// =================================
// Borrar cuenta del usuario
// =================================
router.delete('/:accountNumber', checkJwt, bankAccountController.deleteAccountNumber.bind(bankAccountController));

export default router;