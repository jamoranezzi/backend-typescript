import "reflect-metadata";
import Container from "typedi";
import { Router } from "express";
import { checkJwt } from "../middleware/CheckJwt";
import { UserProductController } from "../controllers/UserProductController";

const router = Router();

// CDI
const userProductController = Container.get(UserProductController);
//

// =================================
// Asociar producto a un cliente
// =================================
router.post('/', checkJwt, userProductController.addProductToUser.bind(userProductController));

// =================================
// Obteners todos los productos permitidos de un Usuario
// =================================
router.get('/all', checkJwt, userProductController.getAllByUser.bind(userProductController));


export default router;