import "reflect-metadata";
import Container from "typedi";
import { Router } from "express";
import { checkJwt } from "../middleware/CheckJwt";
import { ProducRequestController } from "../controllers/ProductRequestController";

const router = Router();

// CDI
const productRequestController = Container.get(ProducRequestController);
//

// =================================
// Crear Solicitud de producto de un Usuario
// =================================
router.post('/', checkJwt, productRequestController.addProductRequest.bind(productRequestController));

// =================================
// Obtener todas las solicitudes de un Usuario
// =================================
router.get('/all', checkJwt, productRequestController.getAllRequestByUser.bind(productRequestController));

// =================================
// Obtener informacion de solicitudes de un Usuario
// =================================
router.get('/:id', checkJwt, productRequestController.getRequestById.bind(productRequestController));

// =================================
// Eliminar solicitud de nuevo Producto
// =================================
router.delete('/:id', checkJwt, productRequestController.deleteProductRequest.bind(productRequestController));

export default router;