import "reflect-metadata";
import Container from "typedi";
import { Router } from "express";
import { AuthController } from "../controllers/AuthController";

const router = Router();

// CDI
const authController = Container.get(AuthController);
//

// =================================
// Login de Usuario
// =================================
router.post('/login', authController.login.bind(authController));

export default router;
