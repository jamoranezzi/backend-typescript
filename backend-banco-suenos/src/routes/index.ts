import user from "./UserRoute";
import product from "./ProductRoute";
import bankAccount from "./BankAccountRoute";
import transaction from "./TransactionRoute";
import userProduct from "./UserProductRoute";
import productRequest from "./ProductRequestRoute";
import auth from "./AuthRoute"
import { Router } from "express";

const routes = Router();

// path inicial Front
routes.get("/", (req, res) => {
    res.render("index");
});

routes.use("/auth", auth);
routes.use("/user", user);
routes.use("/product", product);
routes.use("/account", bankAccount);
routes.use("/tran", transaction);
routes.use("/prods", userProduct);
routes.use("/new/prod", productRequest);

export default routes;