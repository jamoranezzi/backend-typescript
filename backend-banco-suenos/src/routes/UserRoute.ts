import "reflect-metadata";
import { Container  } from "typedi";
import { Router } from "express";
import { checkJwt } from "../middleware/CheckJwt";
import { UserController } from "../controllers/UserController";

const router = Router();

// CDI
const userController = Container.get(UserController);
//

// =================================
// Obtener todos los usuarios
// =================================
router.get('/all', checkJwt, userController.getAllUsers.bind(userController));

// =================================
// Obtener usuario
// =================================
router.get('/:cedula', checkJwt, userController.getUser.bind(userController));

// =================================
// Crear un Usuario
// =================================
router.post('/',  userController.createUser.bind(userController));

// =================================
// Actualizar un Usuario
// =================================
router.put('/', checkJwt, userController.updateUser.bind(userController));

// =================================
// Eliminar usuario
// =================================
router.delete('/:cedula', checkJwt, userController.deleteUser.bind(userController));

export default router;
