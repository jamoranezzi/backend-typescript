import "reflect-metadata";
import Container from "typedi";
import { Router } from "express";
import { checkJwt } from "../middleware/CheckJwt";
import { TransactionController } from "../controllers/TransactionController";

const router = Router();
// CDI
const transactionController = Container.get(TransactionController);
//

// =================================
// Crear Transaccion
// =================================
router.post('/', checkJwt, transactionController.createTransaction.bind(transactionController));

// =================================
// Obtener Todas las transacciones de una cuenta
// =================================
router.get('/all', checkJwt, transactionController.getAllTransactionsByAccount.bind(transactionController));

export default router;